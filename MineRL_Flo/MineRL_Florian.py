import gym
import minerl
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import logging
import custom_environments
from DQN import *
from ExperienceReplay import ExperienceReplay
import numpy as np

#logging.basicConfig(level=logging.DEBUG)


#Initialisation de l'environnement
env = gym.make('Mineline-v0')
print('Action space:', env.action_space)
print('Observation space:', env.observation_space)

#Initialisation des paramètres
action = env.action_space.sample()
action_keys = list(action.keys())
n_acts = len(action_keys)

n_episodes = 1500    #nbr épisodes max pour training
max_steps = 300    #nbr step max par épisode
er_capacity = 2000    #capacité de notre ER

update_freq = 10    #fréquence de mise à jour de notre réseau
target_update_delay = 500    #fréquence de mise à jour du target_model
n_anneal_steps = 50000    #paramètre pour contrôler l'exploration
epsilon = lambda step: np.clip(1 - 0.9 * (step/n_anneal_steps), 0.25, 1)   #fonction permettant de contrôler lexploration
train_batch_size = 32    #taille d'un batch d'entraînement
learning_rate = 0.00025
print_freq = 10

#Initialisation de l'ER, du model, du target_model
er = ExperienceReplay(er_capacity)
model = DQN(n_acts)
model.load_state_dict(torch.load('models/episode261.pt'))
target_model = copy.deepcopy(model)
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
all_rewards = []
global_step = 41189


for episode in range(263,n_episodes):

    prev_frames = []
    obs = env.reset()
    obs, prev_frames = preprocess_obs(obs['pov'], prev_frames)
    
    episode_reward = 0
    step = 0


    while step < max_steps:

        #Début du step
        #Choix d'une action random pour explorer ou non en fonction de la valeur d'epsilon
        
        if np.random.rand() < epsilon(global_step):
            ind = np.random.randint(0,3)
            act = env.action_space.noop()
            act[action_keys[ind]]=1
        else:
            obs_tensor = torch.tensor(np.array([obs])).float()
            q_values = model(obs_tensor)[0]
            q_values = q_values.cpu().detach().numpy()
            ind = np.argmax(q_values)
            act = env.action_space.noop()
            act[action_keys[ind]]=1
        
        act_array = [act[action_key] for action_key in action_keys]

        #Récuperation des données suite à l'action faite
        next_obs, reward, done, _ = env.step(act)
        episode_reward += reward


        next_obs, prev_frames = preprocess_obs(next_obs['pov'], prev_frames)
        er.add_step([obs, act_array, reward, next_obs, int(done)])
        obs = next_obs
        
        #Train sur un batch
        if global_step % update_freq == 0:
            obs_data, act_data, reward_data, next_obs_data, terminal_data = er.sample(train_batch_size)
            model.train_on_batch(target_model, optimizer, obs_data, act_data,
                                 reward_data, next_obs_data, terminal_data)
        
        #Mise à jour du target_model
        if global_step and global_step % target_update_delay == 0:
            target_model = copy.deepcopy(model)
        
        #Fin du step
        step += 1
        global_step += 1
        
        if done:
            break

    all_rewards.append(episode_reward)

    if episode % print_freq == 0:
        print('Episode #{} | Step #{} | Epsilon {:.2f} | Avg. Reward {:.2f}'.format(
            episode, global_step, epsilon(global_step), np.mean(all_rewards[-print_freq:])))

    #Tracking des résultats    
    histo = open("historique_train2.txt", "a")   
    histo.write("Episode #{} | Step #{} | Epsilon {:.2f} | Reward {:.2f} | Done {} | Last Reward {}\n".format(
        episode, global_step, epsilon(global_step), episode_reward, done, reward))
    histo.close()

    if reward > 100 and episode_reward > 0:
        save = model.state_dict()
        path = 'models/episode' + str(episode) + '.pt'
        torch.save(save,path)

env.close()

