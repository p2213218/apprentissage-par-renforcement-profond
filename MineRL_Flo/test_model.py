#Pour tester un modèle

import gym
import minerl
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import logging
import custom_environments
from DQN import *
from ExperienceReplay import ExperienceReplay
import numpy as np


#Initialisation de l'environnement
env = gym.make('Mineline-v0')
print('Action space:', env.action_space)
print('Observation space:', env.observation_space)

#Initialisation des paramètres
action = env.action_space.sample()
action_keys = list(action.keys())
n_acts = len(action_keys)

#Initialisation du MODEL
model = DQN(n_acts)
model.load_state_dict(torch.load('models/episode261.pt'))


prev_frames = []
obs = env.reset()
obs, prev_frames = preprocess_obs(obs['pov'], prev_frames)

episode_reward = 0
step = 0


while step < 300:

    #Début du step
    #Choix d'une action
    obs_tensor = torch.tensor(np.array([obs])).float()
    q_values = model(obs_tensor)[0]
    q_values = q_values.cpu().detach().numpy()
    ind = np.argmax(q_values)
    act = env.action_space.noop()
    act[action_keys[ind]]=1
    
    act_array = [act[action_key] for action_key in action_keys]

    #Récuperation des données suite à l'action faite
    next_obs, reward, done, _ = env.step(act)
    episode_reward += reward


    next_obs, prev_frames = preprocess_obs(next_obs['pov'], prev_frames)
    obs = next_obs
    
    #Fin du step
    step += 1
    
    if done:
        break

env.close()
print(episode_reward)
