import time
import numpy as np
import matplotlib.pyplot as plt
import torch
from torch import nn
import copy
import cv2

N_FRAMES = 4
n_acts = 3


#Fonction pour traiter notre observation, on la reshape en plus petit
#On la met en noir et blanc et on normalise
def filter_obs(obs, resize_shape=(64, 64), crop_shape=None):
    assert(type(obs) == np.ndarray), "The observation must be a numpy array!"
    assert(len(obs.shape) == 3), "The observation must be a 3D array!"

    obs = cv2.resize(obs, resize_shape, interpolation=cv2.INTER_LINEAR)
    obs = cv2.cvtColor(obs, cv2.COLOR_BGR2GRAY)
    obs = obs / 255.
    
    return obs

#Fonction permettant de stacker plusieurs observations que l'on va ensuite mettre en entrée du modèle
def get_stacked_obs(obs, prev_frames):
    if not prev_frames:
        prev_frames = [obs] * (N_FRAMES - 1)
        
    prev_frames.append(obs)
    stacked_frames = np.stack(prev_frames)
    prev_frames = prev_frames[-(N_FRAMES-1):]
    
    return stacked_frames, prev_frames

#Fonction qui fait les deux fonctions précédentes en même temps
def preprocess_obs(obs, prev_frames):
    filtered_obs = filter_obs(obs)
    stacked_obs, prev_frames = get_stacked_obs(filtered_obs, prev_frames)
    return stacked_obs, prev_frames



#Création de notre DQN
class DQN(torch.nn.Module):
    
    #Détail de notre DQN
    def __init__(self, n_acts): 
        super(DQN, self).__init__() 
        
        self.layer1 = nn.Sequential(        #input size(64,64) after preprocessing
            nn.Conv2d(N_FRAMES, 32, kernel_size=6, stride=3, padding=0),
            nn.ReLU())                      #output size(20,20)
        self.layer2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0),
            nn.ReLU())                      #output size(9,9)
        self.layer3 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=0),
            nn.ReLU())                      #output size(7,7)
        self.layer4 = nn.Sequential(
            nn.Linear(64 * 7 * 7, 512),
            nn.ReLU())
        self.layer5 = nn.Sequential(
            nn.Linear(512, n_acts))

    #Utilisation du DQN
    def forward(self, obs): 
        q_values = self.layer1(obs)
        q_values = self.layer2(q_values)
        q_values = self.layer3(q_values)
        q_values = q_values.view(-1, 64 * 7 * 7)
        q_values = self.layer4(q_values)
        q_values = self.layer5(q_values)
        return q_values
    
    #Entraînement sur un batch de données
    def train_on_batch(self, target_model, optimizer, obs, acts, rewards, next_obs, 
                       terminals, gamma=0.99):
        
        #Calcul des next_act avec le model de base
        next_q_values = self.forward(next_obs)
        max_next_acts = torch.max(next_q_values, dim=1)[1].detach()
        
        #Calcul des q_values avec le target model
        target_next_q_values = target_model.forward(next_obs)
        max_next_q_values = target_next_q_values.gather(index=max_next_acts.view(-1, 1), dim=1)
        max_next_q_values = max_next_q_values.view(-1).detach()        
        
        #Calcul de la "vraie" q value
        terminal_mods = 1 - terminals
        actual_qs = rewards + terminal_mods * gamma * max_next_q_values

        #Calcul de la prédiction    
        pred_qs = self.forward(obs)
        pred_qs = pred_qs.gather(index=acts, dim=1)
        pred_qs = torch.max(pred_qs, 1).values
        
        #Calcul de l'erreur
        loss = torch.mean((actual_qs - pred_qs) ** 2)
        
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()